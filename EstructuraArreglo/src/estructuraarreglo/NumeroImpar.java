/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuraarreglo;

/**
 *
 * @author estudiante
 */
public class NumeroImpar {
    int numeros [];

    public NumeroImpar(int[] numeros) {
        this.numeros = numeros;
    }
  
    public NumeroImpar(){
    }
    public int numeroImpar(){
        int num1 = 0;
        int num2 = 0;
        int suma = 0;
        
        for (int i = 0; i <= numeros.length; i++) {
            if(i%2 == 0){
                num1 += 1;
            }else{
                num2 += 1;
                suma += i; 
            }
        }
        return suma;
    }
}
