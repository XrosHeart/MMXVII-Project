/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuraarreglo;

/**
 *
 * @author estudiante
 */
public class EstructuraArreglo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] arreglo = new int[5];
        arreglo[2]=10;
        //Si pusiera arreglo[5]= 75 por ejem, me dará error porque la lista es
        //de 5 elementos pero inicia en 0 y termina en 4 osea (n-1).
        System.out.println(arreglo.length);
        
        arreglo[arreglo.length-1]=8;
        
        System.out.println(arreglo[0]); 
        System.out.println(arreglo[1]); 
        System.out.println(arreglo[2]); 
        System.out.println(arreglo[3]); 
        System.out.println(arreglo[4]);
        
        // En vez de hacer esto se hace un ciclo for
        
        System.out.println("Como un imprimir un arreglo");
        for (int i = 0; i < arreglo.length; i++) {
            System.out.println(i+" ");
        }
        System.out.println("");
        
        int x [] = {1,2,3,4,5,6,7,8,9};
        Logica log1 = new Logica(x);
        System.out.println("El promedio es: " + log1.promedio());
        
        int x2 [] = {1,2,3,4,5,6,7,8,9};
        NumeroImpar num = new NumeroImpar(x2);
        System.out.println("La suma de los números impares es: " + num.numeroImpar());
        
        int x3 [] = {1,2,3,4,5};
        Logica may = new Logica(x3);
        System.out.println("El número mayor es: " + may.buscarMayor(x3));
        System.out.println("El arreglo invertido es: " + may.imprimir(may.invertir(x3)));
        System.out.println("Rotar: " + may.imprimir(may.rotarDer(x3)));
        
        // Paso de parametros por valor y paso de parametros por referencia
    }
    
}
