/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuraarreglo;

/**
 *
 * @author estudiante
 */
public class Logica {
    
    /**
     * Calcula el promedio de los elementos de un arreglo
     * @param arreglo Arreglo con los elementos
     * @return double con el promedio
     */
    public double promedio(int arreglo[]){
        double promedio = 0;
        for (int i = 0; i < arreglo.length; i++) {
            promedio += arreglo[i];
        }
        return promedio / arreglo.length;
    }
    
    /**
     * @param arreglo
     * @return
     */
    public int buscarMayor(int arreglo []){
        int mayor = 0;
        
        for (int i = 0; i < arreglo.length; i++) {
            if(arreglo[i] > mayor){
                mayor = arreglo[i];
            }
        }
        return mayor;
    }
    
    public boolean buscarElemento(int arreglo[], int elemento){
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == elemento){
                return true;
            }             
        }
        return false;
    }
    
    public int buscarPosicion(int arreglo [], int elemento){
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == elemento){
                return i;
            }          
        }
        return -1;
    }
    public int [] invertir(int arreglo[]){
        int resultado [] = new int[arreglo.length];
        int x = resultado.length - 1;
        for (int i = 0; i < arreglo.length; i++) {
            resultado[x] = arreglo [i];
            x --;
        }
        return resultado;
    }
    
    public int [] rotarDer(int arreglo []){
        int respaldo = arreglo[arreglo.length-1];
        for (int i = arreglo.length-2; i >= 0; i--) { //OJO, Este ciclo es distinto a todos los demás
            arreglo[i+1] = arreglo[i];
        }
        arreglo[0] = respaldo;
        return arreglo;
    }
    
    /**
     * Concatena en un String los valores del arreglo
     * @param arreglo Arreglo con los datos
     *
    */
    public String imprimir(int arreglo []){
        String res = "";
        for (int i = 0; i < arreglo.length; i++) {
            res += arreglo[i] + ", "; 
        }
        res += "\b\b"; //Borra el último caracter, como son dos backspaces se borran 2
        return res;
    }
    
    int numeros [];

    public Logica(int[] numeros) {
        this.numeros = numeros;
    }
    
    public double promedio(){
        int suma = 0;
        for (int i = 0; i < numeros.length; i++) {
            suma += numeros[i];
        }
        double promedio = (suma / numeros.length);
        return promedio;                
    }       
}
