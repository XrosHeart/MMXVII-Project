/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuraarreglo;

/**
 *
 * @author estudiante
 */
public class Persona {
    private int cedula;
    private String apellido;
    private String nombre;
    public Persona(){        
    }
    
    public Persona(int cedula, String nombre, String apellido){
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    

    public int getCedula() {
        return cedula;
    }

    public String getApellido() {
        return apellido;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
}
