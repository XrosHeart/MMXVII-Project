/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerCiudad;

/**
 *
 * @author estudiante
 */

public class Ciudad {
    private int tarifa;
    private String nombre;

    public Ciudad(int tarifa, String nombre) {
        this.tarifa = tarifa;
        this.nombre = nombre;
    }
    
    public int getTarifa() {
        return tarifa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setTarifa(int tarifa) {
        this.tarifa = tarifa;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Ciudad{" + "tarifa=" + tarifa + ", nombre=" + nombre + '}';
    }
    
}
