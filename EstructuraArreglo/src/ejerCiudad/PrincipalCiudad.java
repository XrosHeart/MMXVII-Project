/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerCiudad;

import javax.swing.JOptionPane;

/**
 *
 * @author estudiante
 */
public class PrincipalCiudad {
    public static Ciudad[] ciudades = new Ciudad[10];
    public static void main(String[] args) {
        Ciudad SanJose = new Ciudad(5,"San José");
        Ciudad Alajuela = new Ciudad(3,"Alajuela");
        Ciudad Heredia = new Ciudad(5,"Heredia");
        Ciudad Cartago = new Ciudad(6,"Cartago");
        Ciudad Puntarenas = new Ciudad(8,"Puntarenas");
        Ciudad ManuelAntonio = new Ciudad(10,"Manuel Antonio");
        Ciudad Siquirres = new Ciudad(4,"Siquirres");
        Ciudad Limon = new Ciudad(10,"Limón");
        Ciudad Guapiles = new Ciudad(9,"Guápiles");
        Ciudad PtoViejo = new Ciudad(4,"Pto. Viejo");
        ciudades [0] = SanJose;
        ciudades [1] = Alajuela;
        ciudades [2] = Heredia;
        ciudades [3] = Cartago;
        ciudades [4] = Puntarenas;
        ciudades [5] = ManuelAntonio;
        ciudades [6] = Siquirres;
        ciudades [7] = Limon;
        ciudades [8] = Guapiles;
        ciudades [9] = PtoViejo;
        
    JOptionPane.showMessageDialog(null,"¡Bienvenido!\nSeleccione Aceptar"
            + " para continuar","Transportes H.B",JOptionPane.PLAIN_MESSAGE);
    String menuPrincipal = "Seleccione una de las siguientes opciones\n1. "
            + "Viajar\n2. Preguntar por monto disponible\n3. Salir";
    String menuZonas = "Seleccione una de las siguientes Zonas del país\n"
            + "1. Valle Central\n2. Zona Sur\n3. Caribe\n4. Volver";
    String menuValleCen = "Seleccione una de las siguientes opciones\n"
            + "1. San José $5\n2. Alajuela $3\n3. Heredia $5\n4. Cartago $6"
            + "\n5. Volver";
    String menuZonaSur = "Seleccione una de las siguientes opciones\n"
            + "1. Puntarenas $8\n2. Manuel Antonio $ 10\n3. Volver";
    String menuCaribe = "Seleccione una de las siguientes opciones"
            + "1. Siquirres $4\n 2. Limón $10\n3. Guapíles $9\n4. Pto. Viejo $4"
            + "\n5. Volver";
    
    while(true){
        int opcMenuPrin = Integer.parseInt(JOptionPane.showInputDialog(menuPrincipal));
        if(opcMenuPrin == 1){
            while(true){
                int opcMenuZonas = Integer.parseInt(JOptionPane.showInputDialog(menuZonas));
                if(opcMenuZonas == 1){
                    while(true){
                        int opcMenuValle = Integer.parseInt(JOptionPane.showInputDialog(menuValleCen));
                        if(opcMenuValle == 1){
                            break;
                        }
                    }
                }
            }
        }else if(opcMenuPrin == 2){
            LogicaCiudad c = new LogicaCiudad();
            int dinero = Integer.parseInt(JOptionPane.showInputDialog("Digite el dinero"
                    + " disponible"));
            System.out.println("Usted puede viajar a las siguientes "
                    + "ciudades: " + c.ciudadesDisponibles(dinero));
        }else{
            break;
        }
    }  
}
}