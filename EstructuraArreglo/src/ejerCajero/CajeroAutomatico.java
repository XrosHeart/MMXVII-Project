/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerCajero;

/**
 *
 * @author estudiante
 */
public class CajeroAutomatico {
    private double saldo;
    private int[] billetes = new int[6];
    private int[] dinero = new int[6];

    public CajeroAutomatico(){
        saldo = 1000;
        billetes[0] = 10000;
        billetes[1] = 5000;
        billetes[2] = 2000;
        billetes[3] = 1000;
        billetes[4] = 500;
        billetes[5] = 100;
        dinero[0] = 0;
        dinero[1] = 0;
        dinero[2] = 0;
        dinero[3] = 0;
        dinero[4] = 0;
        dinero[5] = 0;
               
    }

    public double getSaldo() {
        return saldo;
    }
    public void deposito(double monto){
        saldo += monto;
    }
    public int retiro(){
        return 0;
    }
}
