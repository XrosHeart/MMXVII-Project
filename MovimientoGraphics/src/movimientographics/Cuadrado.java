/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movimientographics;

import java.awt.Color;
import java.awt.event.KeyEvent;

/**
 *
 * @author Fernando
 */
public class Cuadrado {
    private Color color;
    public final int LADO = 40;
    public final int C_LADOS = 4;
    
    private int direccion;
    private int x;
    private int y;
    private int speed;
    
    public Cuadrado(){
        
    }
    
    public Cuadrado(Color color, int x, int y){
        this.color = color;
        this.x = x;
        this.y = y;
    }
    
    public void mover(int tecla){
        switch(tecla){
            case KeyEvent.VK_UP:
                direccion = 0;
                break;
            case KeyEvent.VK_DOWN:
                direccion = 1;
                break;
            case KeyEvent.VK_LEFT:
                direccion = 2;
                break;
            case KeyEvent.VK_RIGHT:
                direccion = 3;
                break;
            case KeyEvent.VK_S:
                direccion = 0;
                break;
            case KeyEvent.VK_A:
                speed ++;
                break;
            case KeyEvent.VK_D:
                speed --;
        }
    }
    
    public void mover(){
        switch(direccion){
            case 1:
                y -= speed;
                break;
            case 2:
                y += speed;
                break;
            case 3:
                x -= speed;
                break;
            case 4:
                x += speed;
                break;
        }
    }
    
    public void incSpeed(){
        speed ++;
    }
    
    public void decSpeed(){
        speed --;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public int getLADO() {
        return LADO;
    }

    public int getC_LADOS() {
        return C_LADOS;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
}
