/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana.u;

/**
 *
 * @author estudiante
 */
public class PrincipalSemanaU {
    public static void main(String [] args){
        Circunferencia rueda = new Circunferencia();
        Circunferencia moneda = new Circunferencia();
        
        rueda.modificarRadio(10.2);
        moneda.modificarRadio(1.4);
        
        System.out.println("El área de la rueda es: " + rueda.CalcularArea()
        + " cm2");
        System.out.println("El área de la moneda es: " + moneda.CalcularArea()
        + " cm2");
        System.out.println("El perimetro de la rueda es: " 
        + rueda.CalcularPerimetro() + " cm");
        System.out.println("El perimetro de la moneda: " 
        + moneda.CalcularPerimetro() + " cm");
        System.out.println("HASTA AQUÍ EL NUMERO UNO\n\n\n");
        
        Rectangulo ventana = new Rectangulo();
        Rectangulo pared = new Rectangulo();
        
        ventana.modificarAncho(1.05);
        ventana.modificarLargo(2.50);
        pared.modificarAncho(3.75);
        pared.modificarLargo(7.85);
        
        double areaTotVentana = ventana.calcularArea();
        double areaTotPared = pared.calcularArea();
        double areaTotPintar = areaTotPared - areaTotVentana;
        int tiempoTotPintar = ((int)(areaTotPintar) * 10);
        int horas = tiempoTotPintar / 60;
        int minutos = tiempoTotPintar % 60;
        System.out.println("El área de la ventana es: " + areaTotVentana
        + " m2\nEl área total de la pared es: " + areaTotPared
        + " m2\nEl área total a pintar es de: " + areaTotPintar
        + " m2\nEl tiempo total será aprox de: " + horas
        + " horas y " + minutos + " miutos\nHASTA AQUÍ LA SDA\n\n\n");
        
        Fecha fecha = new Fecha();
        System.out.println("La fecha es: " + fecha.getFechaCorta()
        + "\n\nHASTA AQUÍ LA 3RA, SI QUIERE HAGA MÁS PRUEBAS LUEGO\n\n");
        
        Gasolinera gas = new Gasolinera();
        System.out.println(gas.calcularCobro(1));
    }
    
    
}