/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semana.u;

/**
 *
 * @author estudiante
 */
public class Fecha {
    private int dia;
    private int mes;
    private int anho;

    public int getDia() {
        return dia;
    }

    public int getMes() {
        return mes;
    }

    public int getAnho() {
        return anho;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public void setAnho(int anho) {
        this.anho = anho;
    }
    
    public Fecha (){
        dia = 06;
        mes = 06;
        anho = 2006;
    }
    
    public Fecha (int dia, int mes, int anho){
        this.dia = dia;
        this.mes = mes;
        this.anho = anho;
    }
    
    public void modificarFecha (int dia, int mes, int anho){
        this.dia = dia;
        this.mes = mes;
        this.anho = anho;
    }
    
    public String mesLetra (int dia, int mes, int anho){
        String mes2 = "";
        if (mes == 1){
            mes2 = (dia + " de Enero del " + anho);
        } else if (mes == 2) {
            mes2 = (dia + " de Febrero del " + anho);
        } else if (mes == 3) {
            mes2 = (dia + " de Marzo del " + anho);
        } else if (mes == 4) {
            mes2 = (dia + " de Abril del " + anho);
        } else if (mes == 5) {
            mes2 = (dia + " de Mayo del " + anho);
        } else if (mes == 6) {
            mes2 = (dia + " de Junio del " + anho);
        } else if (mes == 7) {
            mes2 = (dia + " de Julio del " + anho);
        } else if (mes == 8) {
            mes2 = (dia + " de Agosto del " + anho);
        } else if (mes == 9) {
            mes2 = (dia + " de Septiembre del " + anho);
        } else if (mes == 10) {
            mes2 = (dia + " de Octubre del " + anho);
        } else if (mes == 11) {
            mes2 = (dia + " de Noviembre del " + anho);
        } else if (mes == 12) {
            mes2 = (dia + " de Diciembre del " + anho);
        } 
        return mes2;
    }
}
