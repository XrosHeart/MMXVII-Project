/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciclos;

import javax.swing.JOptionPane;

/**
 *
 * @author estudiante
 */
public class Ciclos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Ejemplos de while
//        int contador = 1;
//        while (contador <= 10){
//            System.out.println(contador);
//            contador ++;
//        }
//        int contador2 = 10;
//        while (contador2 >= 1){
//            System.out.println(contador2);
//            contador2 --;
//        }
//        int tabla = Integer.parseInt(JOptionPane.showInputDialog("Digite la tabla que desea"));
//        int contador3 = 0;
//        while (contador3 <= 9){
//            System.out.println(tabla + " * " + contador3 + " = " + (tabla * contador3) );
//            contador3 ++;
//        }
//        int sum = 0;
//        int contador4 = 1;
//        while (contador4 <= 10){
//            int num = Integer.parseInt(JOptionPane.showInputDialog("Digite el número: "));
//            contador4 ++;
//            sum = (sum + num);
//        }
//        System.out.println("La sumatoria total de los números ingresados es: " + sum);
//        
//        while (true){
//            int num = Integer.parseInt(JOptionPane.showInputDialog("Digite el número: "));
//            String ope = JOptionPane.showInputDialog("Digite el operador: ");
//            int num2 = Integer.parseInt(JOptionPane.showInputDialog("Digite el otro número: "));
//            switch (ope){
//                case "+":
//                    System.out.println(num + " + " + num2 + " = " + (num + num2));
//                    break;
//                case "-":
//                    System.out.println(num + " - " + num2 + " = " + (num - num2));
//                    break;
//                case "/":
//                    System.out.println(num + " / " + num2 + " = " + (num / num2));
//                    break;
//                case "*":
//                    System.out.println(num + " * " + num2 + " = " + (num * num2));
//                    break;
//            }
//            break;
//        }
//        while (true){
//            int op = Integer.parseInt(JOptionPane.showInputDialog("Menú: \n1. Suma\n2. Resta\n"
//                                                                  + "3.Multiplicación\n4.División\n"
//                                                                  + "5.Salir\n\nDigite la opción "
//                                                                  + "que desee: "));
//            if (op == 5){
//                break;
//            }
//            int num = Integer.parseInt(JOptionPane.showInputDialog("Digite el primer número: "));
//            int num2 = Integer.parseInt(JOptionPane.showInputDialog("Digite el segundo número: "));
//            int resu = 0;
//            if (op == 4){
//                System.out.println("El resultado de la división es: " + (num / num2));
//            } else if (op == 3){
//                System.out.println("El resultado de la multiplicación es: " + (num * num2));
//            } else if (op == 2){
//                System.out.println("El resultado de la resta es: " + (num - num2));
//            } else if (op == 1){
//                System.out.println("El resultado de la suma es: " + (num + num2));
//            }
//        }
//        // DOWHILES
//        int con = 1;
//        do{
//            System.out.println(con);
//            con++;
//        } while (con <= 10);
        
        // FOR
        for(int x = 0; x <= 10; x++){
            System.out.println(x);
        }
        for (int x = 10; x >= 0; x--) {
            System.out.println(x);
        }
        for (int x = 1; x <= 1000;x++) {
            if (x % 7 == 0){
                System.out.println(x);
            }
            
        }
    }
}