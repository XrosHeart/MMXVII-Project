/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sintaxis;

import javax.swing.JOptionPane;

/**
 *
 * @author estudiante
 */
public class Sintaxis {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Hola Mundo!");
        System.out.println(5 == 5);
        System.out.println(5 == 6);
        System.out.println(5 != 6);
        System.out.println(5 > 6);
        System.out.println(5 <= 6);
        System.out.println(6 <= 6);

        boolean X = true;
        boolean Y = false;
        System.out.println("X & Y: " + (X && Y));

        X = true;
        Y = true;
        System.out.println("X & Y: " + (X && Y));

        int edad = 18;
        //Formatear el código: ALT + SHIFT + F
        boolean res = edad >= 18;
        System.out.println("Estructuras de control");

        if (edad >= 18) {
            System.out.println("Tiene permiso");
        } else {
            System.out.println("No tiene permiso");
        }
        
        int x = Integer.parseInt(JOptionPane.showInputDialog("Digite un número"));
        if (x % 2 == 0) {
            System.out.println("Variable x = " + x + " es par");
        }
        else {
            System.out.println("Variable x = "+ x + " es impar");
        }
        
        int x1 = Integer.parseInt(JOptionPane.showInputDialog("Digite el primer número: "));
        int y = Integer.parseInt(JOptionPane.showInputDialog("Digite el segundo número: "));
        
        if (x1 < y) {
            System.out.println("Variable x = " + x + " es menor que y = " + y);
        }
        else if (x1 > y) {
            System.out.println("Variable x = " + x + " es mayor que y = " + y);
        }
        else {
            System.out.println("Variable x = " + x + " y = " + y + " son iguales");
        }
        
        int x2 = Integer.parseInt(JOptionPane.showInputDialog("Digite un numero - ó +"));
        
        if (x2 >= 0) {
            System.out.println("La variable " + x2 + " es positiva");
        }
        
        else {
            System.out.println("La variable " + x2 + "es negativa");
        }
        
        int x3 = Integer.parseInt(JOptionPane.showInputDialog("Digite un número HUEVON!"));
        
        if (x3 > 0) {
            if (x3 % 5 == 0) {
                System.out.println("Es divisible");
            }
            else {
                System.out.println("No es divisible");
            }
        }   
        //Ejercicio #2
        int aleatorio = (int)(Math.random()*6)+1; //Crear un random
        System.out.println(aleatorio);
        
        //Ejercicio #3
        int pasajeros = Integer.parseInt(JOptionPane.showInputDialog("Digite el número de pasajeros"));
        int ejes = Integer.parseInt(JOptionPane.showInputDialog("Digite el número de ejes"));
        int costoVehiculo = Integer.parseInt(JOptionPane.showInputDialog("Digite el valor del vehiculo"));
        double impPasajeros;
        double impEjes;
        double imp = costoVehiculo * 0.01;
                
        if (pasajeros < 20) {
            impPasajeros = costoVehiculo * 0.01;
            }
        else if (pasajeros > 20 && pasajeros < 60) {
            impPasajeros = costoVehiculo * 0.05;
            }
        else {
            impPasajeros = costoVehiculo * 0.08;
            }
        
        if (ejes == 2) {
            impEjes = costoVehiculo * 0.05;
            }
        else if (ejes == 3) {
            impEjes = costoVehiculo * 0.10;
            }
        else {
            impEjes = costoVehiculo * 0.15;
            }
        
        double costoTotal = impPasajeros + impEjes + imp + costoVehiculo;
        System.out.println("Los impuestos de venta son: " + imp);
        System.out.println("Los impuestos de ejes son: " + impEjes);
        System.out.println("Los impuestos de la cantidad de pasajeros es: " + impPasajeros);
        System.out.println("El costo neto del vehiculo es de: " + costoVehiculo);
        System.out.println("El costo total del vehiculo es: " + costoTotal + " colones i.v.i");
        int nota = Integer.parseInt(JOptionPane.showInputDialog("Nota del examen: "));
          
        if (nota < 70) {
            System.out.println("Su nota: " + nota + " es INSUFICIENTE, REPROBADO! IDIOTA!");
          }
        else if (nota > 70 && nota < 80) {
            System.out.println("Su nota: " + nota + " está BIEN.");
          }
        else if (nota > 80 && nota < 90) {
            System.out.println("Su nota: " + nota + " es SUPERIOR.");
          }
        else if (nota > 90 && nota <= 100) {
            System.out.println("Su nota: " + nota + " es SOBRESALIENTE!.");
          }
         int canciones = Integer.parseInt(JOptionPane.showInputDialog("Digite el número de canciones"));
         int partituras = Integer.parseInt(JOptionPane.showInputDialog("Digite el número de partituras"));
         if (canciones > 7 && canciones < 10 && partituras == 0) {
             System.out.println("Músico naciente");
            }
         else if (canciones >= 7 && canciones < 10 && partituras >= 1 && partituras <= 5) {
             System.out.println("Músico estelar");
            }
         else if (canciones >= 10 && partituras > 5) {
             System.out.println("Músico consagrado");
            }
         else {
             System.out.println("Músico en formación");
            }
    int dinero = Integer.parseInt(JOptionPane.showInputDialog("Digite el total de dinero"));
    int billullu;
    int monedeichon;
    if (dinero >= 500) {
        billullu = dinero / 500;
        if (billullu > 0) {
            System.out.println("Billullus de 5 tejas: " + billullu);
            dinero -= 500 * billullu;
            }
        }
    if (dinero >= 200) {
        billullu = dinero / 200;
        if (billullu > 0) {
            System.out.println("Billullus de 2 tejas: " + billullu);
            dinero -= 200 * billullu;
            } 
        }
    if (dinero >= 100) {
        billullu = dinero / 100;
        if (billullu > 0) {
            System.out.println("Billullus de 1 teja: " + billullu);
            dinero -= 100 * billullu;
            }
        }
    if (dinero >= 50) {
        billullu = dinero / 50;
        if (billullu > 0) {
            System.out.println("Billullus de 1/2 teja: " + billullu);
                    dinero -= 50 * billullu;
            }
        }
    else if (dinero >= 20) {
        billullu = dinero / 20;
        if (billullu > 0) {
            System.out.println("Billullus de 20: " + billullu);
            dinero -= 20 * billullu;
            }
        
        }
    if (dinero >= 10 ) {
        billullu = dinero / 10;
        if (billullu > 0) {
            System.out.println("Billullus de 10: " + billullu);
            dinero -= 10 * billullu;
            }
        }
    if (dinero >= 5) {
        billullu = dinero / 5;
        if (billullu > 0) {
            System.out.println("Billullus de 5: " + billullu);
            dinero -= 5 * billullu;
            }
        }
    if (dinero >= 2) {
        monedeichon = dinero / 2;
        if (monedeichon > 0) {
            System.out.println("Monedichas de 2: " + monedeichon);
            dinero -= 2 * monedeichon;
            }
        }
    if (dinero >= 1) {
        monedeichon = dinero / 1;
        if (monedeichon > 0) {
            System.out.println("Monedichas de 1: " + monedeichon);
            dinero -= 1 * monedeichon;
           }
        }
    }
}
        