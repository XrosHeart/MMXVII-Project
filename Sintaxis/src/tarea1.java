
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fernando
 */
public class tarea1 {
    public static void main(String[] args) {
        
        double desc;
        double total;
        int precio = Integer.parseInt(JOptionPane.showInputDialog("Digite el total del precio del pasaje"));
        int pasajeros = Integer.parseInt(JOptionPane.showInputDialog("Digite el total de pasajeros"));
        if (pasajeros == 1) {
            int edad = Integer.parseInt(JOptionPane.showInputDialog("Digite su edad"));
            if (edad >= 18 && edad < 30) {
                desc = (precio * 0.078);
                total = (precio - desc);
                System.out.println("Pasajeros = " + pasajeros + "\nEdad = " + edad + 
                        "\nDescuento aplicado por la edad (7.8%) = " + desc + 
                        "\nPrecio neto = " + total + "colones i.v.i");
            } else if (edad >= 30) {
                desc = (precio * 0.1);
                total = (precio - desc);
                System.out.println("Pasajeros = " + pasajeros + "\nEdad = " + edad + 
                        "\nDescuento aplicado por la edad (10.0%) = " + desc + 
                        "\nPrecio neto = " + total + "colones i.v.i");
            } else {
                System.out.println("No se permite el viaje de menores de edad sin acompañantes");
            }
        } if (pasajeros == 2) {
            desc = ((precio * 2) * 0.115);
            total = ((precio * 2) - desc);
            System.out.println("Pasajeros = " + pasajeros + "\nDescuento aplicado (11.5%) = " + desc + 
                        "\nPrecio neto = " + total + "colones i.v.i");
        } if (pasajeros >= 3) {
            desc = ((precio * pasajeros) * 0.15);
            total = ((precio * pasajeros) - desc);
            System.out.println("Pasajeros = " + pasajeros + "\nDescuento (15.0%) = " + desc + 
                        "\nPrecio neto = " + total + "colones i.v.i");
        } if (pasajeros <= 0) {
            System.out.println("Los pasajeros no pueden ser 0");
        }
    }
}
