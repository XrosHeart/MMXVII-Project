/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demographics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Fernando
 */
public class FondoLineas extends JPanel implements KeyListener {

    private int rojo = 0;
    private int verde = 0;
    private int azul = 0;
    private int circuloR = 0;
    private int circuloV = 0;
    private int circuloA = 0;
    private int x1 = 50;
    private int y1 = 50;
    
    public FondoLineas(){
        
        setFocusable(true);
        addKeyListener(this);
        System.out.println("test");
    }
    @Override
    public void paint(Graphics g){
        //super.paint(g);
        int a = getWidth();
        int l = getHeight();
        
        
        g.fillOval(a/2-x1, l/2-y1, 50, 50); //EJERCICIO DEL MOVIMIENTO DEL CIRCULO
        //DEJANDO UNA ESTELA
        //CON ESTO SE LOGRA CENTRAR EL CIRCULO
        
        
        //Degradacion de izq a der
//        for (int i = 0; i < getWidth(); i++) {
//            azul = i * 256 / getWidth();
//            rojo = i * 256 / getWidth();
//            g.setColor(new Color(rojo,verde,azul));
//            g.drawLine(i, 0, i, getHeight());
//            }
//        }
    
        //Degradacion de arriba a abajo
//        for (int i = 0; i < getWidth(); i++) {
//            azul = i * 256 / getWidth();
//            g.setColor(new Color(rojo,verde,azul));
//            g.drawLine(0, i, getWidth(), i);
//        }
        
        //Degradacion en Diagonal
//        for (int i = 0; i < getHeight() + getWidth(); i++) {
//            this.azul = i * 255 / (getHeight() + getWidth());
//            g.setColor(new Color(this.rojo,this.verde,this.azul));
//            g.drawLine(0,i,i,0);
//        }
//        g.setColor(new Color(82,116,161));
//        for (int i = 0; i < getWidth(); i++){ //getWidth() Se obtiene el ancho de la ventana
//           g.drawLine(i, 0, i, getHeight()); //getHeight() Se obtiene el largo de la ventana
//                                             //CADA VEZ QUE SE MAXIMIZA LA PANTALLA IGUAL SE
//                                             //RELLENA TODA LA PANTALLA
//        }                                     
//        g.setColor(new Color(39,166,219));
//        for (int i = 0; i < getWidth(); i++) {
//            g.setColor(new Color(this.rojo,this.verde,this.azul));
//            g.drawLine(i, 0, i, getHeight());
//            g.setColor(new Color(this.circuloR, this.circuloV,this.circuloA));
//            g.fillOval(200,200,50,50);
//        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        System.out.println("KeyTyped: " + ke.getKeyCode());
        repaint();
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        //CON ESTE CODIGO SE LOGRA QUE EL CIRCULO CAMBIE DE COLOR Y EL FONDO TAMBIEN
        //SOLO QUE DEL 1-5 CAMBIA EL FONDO Y DEL 6-0 EL CIRCULO 
//        switch (ke.getKeyCode()) {
//            case KeyEvent.VK_0:
//                this.circuloR = 100;
//                this.circuloV = 100;
//                this.circuloA = 100;
//                break;
//            case KeyEvent.VK_1:
//                this.rojo = 200;
//                this.verde = 100;
//                this.azul = 100;
//                break;
//            case KeyEvent.VK_2:
//                this.rojo = 200;
//                this.verde = 200;
//                this.azul = 100;
//                break;
//            case KeyEvent.VK_3:
//                this.rojo = 200;
//                this.verde = 200;
//                this.azul = 200;
//                break;
//            case KeyEvent.VK_4:
//                this.rojo = 50;
//                this.verde = 100;
//                this.azul = 100;
//                break;
//            case KeyEvent.VK_5:
//                this.rojo = 50;
//                this.verde = 50;
//                this.azul = 100;
//                break;
//            case KeyEvent.VK_6:
//                this.circuloR = 147;
//                this.circuloV = 90;
//                this.circuloA = 11;
//                break;
//            case KeyEvent.VK_7:
//                this.circuloR = 75;
//                this.circuloV = 100;
//                this.circuloA = 100;
//                break;
//            case KeyEvent.VK_8:
//                this.circuloR = 75;
//                this.circuloV = 75;
//                this.circuloA = 100;
//                break;
//            case KeyEvent.VK_9:
//                this.circuloR = 75;
//                this.circuloV = 90;
//                this.circuloA = 125;
//                break;
//            default:
//                break;
//        }
        switch (ke.getKeyCode()) {
            case KeyEvent.VK_UP:
                y1 += 25;
                repaint();
                break;
            case KeyEvent.VK_DOWN:
                y1 -= 25;
                repaint();
                break;
            case KeyEvent.VK_LEFT:
                x1 += 25;
                repaint();
                break;
            case KeyEvent.VK_RIGHT:
                x1 -= 25;
                repaint();
                break;
            default:
                throw new AssertionError();
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        System.out.println("KeyReleased: " + ke.getKeyCode());
    }
}
