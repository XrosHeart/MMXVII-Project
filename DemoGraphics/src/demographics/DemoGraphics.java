/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demographics;

import javax.swing.JFrame;

/**
 *
 * @author Fernando
 */
public class DemoGraphics {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Fondo fondo = new Fondo();//Se crea un objeto de tipo panel personalizado
        FondoLineas fl = new FondoLineas(); //Importacion de la clase FondoLineas
        Triangulo t = new Triangulo();
        JFrame frm = new JFrame();//Creamos un formulario
        //frm.add(fondo);// //Agregamos un panel al formulario ESTE ES EL MONIGOTE
        //frm.add(fl); Panel al formulario de FondoLineas
        //frm.add(t);
        frm.setSize(400, 400);//Modificacion del tamanho de la ventana determinado por pixeles
        frm.setVisible(true); //Con esto se logra observar la ventana, osea que sea Visible
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//Con esto puede cerrar la ventana en la X roja
        frm.setLocationRelativeTo(null); //Con esto centro en la pantalla de NetBeans no del monitor, la ventana
        
    }
    
}
