/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demographics;

import java.awt.Color;
import java.awt.event.KeyEvent;

/**
 *
 * @author Fernando
 */
public class Triangulo{
    private Color color;
    public final int BASE = 100;
    public final int ALTURA = 100;
    public final int C_LADOS = 3;
    
    private int x;
    private int y;
    
    public Triangulo(){
        
    }
    
    public Triangulo(Color color, int x, int y){
        this.color = color;
        this.x = x;
        this.y = y;
        
    }
    
    public void mover(int tecla){
        switch(tecla){
            case KeyEvent.VK_UP:
                y -= 10;
                break;
            case KeyEvent.VK_DOWN:
                y += 10;
                break;
            case KeyEvent.VK_LEFT:
                x -= 10;
                break;
            case KeyEvent.VK_RIGHT:
                x += 10;
                break;
        }
    }
    
    public int[] arregloX(){
        int[]res = new int[C_LADOS];
        res[0] = x;
        res[1] = x + BASE/2;
        res[2] = x + BASE;
        return res;
    }
    
    public int[] arregloY(){
        int[]res = new int[C_LADOS];
        res[0] = y;
        res[1] = y - ALTURA;
        res[2] = y; 
        return res;
    }

    public Color getColor() {
        return color;
    }

    public int getBASE() {
        return BASE;
    }

    public int getALTURA() {
        return ALTURA;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
    
}