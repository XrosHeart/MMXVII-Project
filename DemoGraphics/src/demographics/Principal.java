/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demographics;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Fernando
 */
public class Principal extends JPanel implements KeyListener{
    
    private Cuadrado cuadrado;
    private Rombo rombo;
    private Triangulo triangulo;
    private int figura;
    
    public Principal(){
        cuadrado = new Cuadrado(Color.RED, 150,150);
        rombo = new Rombo(Color.ORANGE, 450, 150);
        triangulo = new Triangulo(Color.YELLOW, 450, 300);
        setFocusable(true);
        addKeyListener(this);
    }
    
    @Override
    public void paint(Graphics g){
        super.paint(g);
        //Pintar el fondo
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, 600, 600);
        //Dibujar Cuadrado
        g.setColor(cuadrado.getColor());
        g.fillRect(cuadrado.getX(), cuadrado.getY(), cuadrado.LADO, cuadrado.LADO);
        //Dibujar Rombo
        g.setColor(rombo.getColor());
        g.fillPolygon(rombo.arregloX(), rombo.arregloY(), rombo.C_LADOS);
        //Dibujar Triangulo
        g.setColor(triangulo.getColor());
        g.fillPolygon(triangulo.arregloX(), triangulo.arregloY(), triangulo.C_LADOS);
        
    }
    
    @Override
    public void keyTyped(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //Seleccionar la figura
        if(ke.getKeyCode() == KeyEvent.VK_C){
            figura = 0;
        }else if(ke.getKeyCode() == KeyEvent.VK_T){
            figura = 1;
        }else if(ke.getKeyCode() == KeyEvent.VK_R){
            figura = 2;
        }if(ke.getKeyCode() >= 48 && ke.getKeyCode() <= 57){
        cambiarColor(ke.getKeyChar());
        }

        switch(figura){
            case 0:
                cuadrado.mover(ke.getKeyCode());
                break;
            case 1:
                triangulo.mover(ke.getKeyCode());
                break;
            case 2:
                rombo.mover(ke.getKeyCode());
                break;
        }    
        repaint();
    }
    
    private void cambiarColor(int tecla){
        Color temp = Color.BLACK;
        switch(tecla){
            case KeyEvent.VK_0:
                temp = Color.RED;
                break;
            case KeyEvent.VK_1:
                temp = Color.BLUE;
                break;
            case KeyEvent.VK_2:
                temp = Color.PINK;
                break;
            case KeyEvent.VK_3:
                temp = Color.GREEN;
                break;
            case KeyEvent.VK_4:
                temp = Color.GRAY;
                break;
            case KeyEvent.VK_5:
                temp = Color.WHITE;
                break;
            case KeyEvent.VK_6:
                temp = Color.LIGHT_GRAY;
                break;
            case KeyEvent.VK_7:
                temp = Color.MAGENTA;
                break;
            case KeyEvent.VK_8:
                temp = Color.ORANGE;
                break;
            case KeyEvent.VK_9:
                temp = Color.CYAN;
                break;
        }
        
        switch(figura){
            case 0:
                cuadrado.setColor(temp);
                break;
            case 1:
                triangulo.setColor(temp);
                break;
            case 2:
                rombo.setColor(temp);
                break;
        }    

    }

    @Override
    public void keyReleased(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
