/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demographics;

import java.awt.Color;
import java.awt.event.KeyEvent;

/**
 *
 * @author Fernando
 */
public class Rombo {
    //UN POLIGONO NECESITA AREGLOS
    public final int D_MENOR = 100;
    public final int D_MAYOR = 100;
    public final int C_LADOS = 4;
    
    private Color color;
    private int x;
    private int y;
    
    public Rombo(){
    }
    
    public Rombo(Color color, int x, int y){
        this.color = color;
        this.x = x;
        this.y = y;
    }
    
    public void mover(int tecla){
        switch(tecla){
            case KeyEvent.VK_UP:
                y -= 10;
                break;
            case KeyEvent.VK_DOWN:
                y += 10;
                break;
            case KeyEvent.VK_LEFT:
                x -= 10;
                break;
            case KeyEvent.VK_RIGHT:
                x += 10;
                break;
        }
    }
    
    public int[] arregloX(){
        int[] res = new int[C_LADOS];
        res[0] = x;
        res[1] = x + D_MENOR/2;
        res[2] = x + D_MENOR;
        res[3] = x + D_MAYOR/2;
        return res;
    }
    
    public int[] arregloY(){
        int[] res = new int[C_LADOS];
        res[0] = y;
        res[1] = y - D_MENOR/2;
        res[2] = y;
        res[3] = y + D_MAYOR/2;
        return res;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getD_MENOR() {
        return D_MENOR;
    }

    public int getD_MAYOR() {
        return D_MAYOR;
    }

    public int getC_LADOS() {
        return C_LADOS;
    }

    public Color getColor() {
        return color;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
