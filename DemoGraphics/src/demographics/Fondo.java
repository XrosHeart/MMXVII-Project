/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demographics;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Fernando
 */
public class Fondo extends JPanel{
    
    @Override
    public void paint(Graphics g){
        super.paint(g);
//        g.setColor(new Color(151,198,130));
//        g.drawLine(10, 10, 100, 100);//Dibujar una linea con Coordenadas en los planos x1 y1 x2 y2
//        g.fillRect(200, 200, 250, 250);
//        
//        g.setColor(Color.black);
//        
//        for (int i = 0; i < 10; i++) {
//            if(i % 2 == 0){
//                g.setColor(Color.black);
//            }else{
//                g.setColor(Color.red);
//            }
//            g.drawLine(10*i + 20, 10 +20 , 100 * i, 100);
//            g.drawOval(10*i,10*i, 50, 50);
//        }

        //Balon
        g.setColor(new Color(192,17,32));
        g.fillOval(600, 575, 100, 100);
        g.setColor(Color.black);
        g.drawOval(600, 575, 100, 100);
        g.drawOval(601, 576, 99, 99);
        g.drawOval(602, 577, 98, 98);
        g.drawOval(603, 578, 97, 97);
        g.drawOval(604, 579, 96, 96);
        
        //Aire detras del Balon
        g.setColor(new Color(114,147,178));
        for (int i = 0; i < 3; i++) {
            g.drawLine(715, 630+i, 745, 640+i);
            g.drawLine(715, 635+i, 745, 645+i);
            g.drawLine(715, 640+i, 745, 650+i);
            g.drawLine(715, 650+i, 745, 660+i);
            g.drawLine(715, 655+i, 745, 665+i);
            g.drawLine(715, 660+i, 745, 670+i);
        }
        //Cabeza
        g.setColor(new Color(249,241,182));
        g.fillOval(800, 100, 150, 150);
        g.setColor(Color.white);
        g.fillOval(850, 150, 30, 20);
        g.setColor(Color.black);
        g.drawOval(850, 150, 30, 20);
        g.setColor(new Color(38,78,137));
        g.fillOval(855, 155, 10, 10);
        g.setColor(Color.black);
        g.fillOval(857, 157, 5, 5);
        
        //Cuello
        g.setColor(new Color(249,241,182));
        for (int i = 0; i < 35; i++) {          
            g.drawLine(885+i, 200, 910+i, 300);
        }
        
        //Hombros
        g.setColor(new Color(195,25,28));
        for (int i = 0; i < 10; i++) {
            g.drawLine(860, 280+i, 980, 300+i);            
        }
        
        //Brazo Der
        g.setColor(new Color(249,241,182));
        for (int i = 0; i < 45; i++) {
            g.drawLine(820, 320+i, 780, 355+i);
        }
        
        //Antebrazo Derecho
        for (int i = 0; i < 40; i++) {
            g.drawLine(780, 355+i, 700, 385+1);
        }
        //Manga Camisa Der.
        g.setColor(new Color(103,8,16));
        for (int i = 0; i < 50; i++) {
            g.drawLine(860, 280+i, 820, 320+i);
        }      
        
        //Brazo Izq
        g.setColor(new Color(249,241,182));
        for (int i = 0; i < 45; i++) {
            g.drawLine(1010, 360+i, 1030, 400+i);
        }
        
        //Antebrazo Izq
        for (int i = 0; i < 40; i++) {
            g.drawLine(1030,400+i,1040,450+i);
        }
        //Manga Camisa Izq.
        g.setColor(new Color(255,88,87));
        for (int i = 0; i < 60; i++) {
            g.drawLine(980, 300+i, 1010, 360+i);
        }
        
        //Camisa Entera.
        g.setColor(new Color(214,18,30));
        for (int i = 0; i < 100; i++) {
            g.drawLine(860+i, 290+i, 850+i, 500);
        }
        
        //Camisa Borde Azul
        g.setColor(new Color(55,66,122));
        for (int i = 0; i < 40; i++) {
            g.drawLine(860, 290, 980, 310+i);
        }
        for (int i = 0; i < 19; i++) {
            g.drawLine(965+i, 310, 950+i, 500);
        }
        //Pantaloneta Azul
        for (int i = 0; i < 60; i++) {
            g.drawLine(860 +i, 500, 820+i, 600);
        }
        for (int i = 0; i < 55; i++) {
            g.drawLine(900+i, 500, 915+i, 600);
        }
        
        //Piernas
        g.setColor(new Color(249,241,182));
        for (int i = 0; i < 45; i++) {
            //Der
            g.drawLine(830+i,601,800+i,650);
            //Izq
            g.drawLine(920+i, 601, 935+i, 650);
        }
        //Bajo Rodillas
        for (int i = 0; i < 44; i++) {
            //Der
            g.drawLine(800+i,650,790+i,680);
            //Izq
            g.drawLine(935+i,650,955+i,680);
        }
        
        //Marco Superior
        for (int i = 0; i < 10; i++) {
            g.setColor(Color.black);
            g.drawLine(20, 200+i, 300, 50+i);
        }
        
        for (int i = 0; i < 10; i++) {
            //Marco Der
            g.drawLine(20+i, 200, 40+i, 500);
            //Marco Izq
            g.drawLine(300+i, 50, 320+i, 350);
        }
    }
}
