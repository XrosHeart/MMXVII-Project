/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulacioncaracteres;

/**
 *
 * @author Fernando
 */
public class ManipulacionCaracteres {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       String str = "Hola Mundo";
       //   |H|o|l|a| |M|u|n|d|o|
       //   |0|1|2|3|4|5|6|7|8|9|
        
        //equals --> Comparar dos Strings, devuelve true si son identicos
        System.out.println(str);
        if(str.equals("Hola Mundo")){
            System.out.println("Son iguales");
        }
        
        //equalsIgnoreCase --> Compara dos Strings sin importar si esta
        //en minus o Mayus
        if(str.equalsIgnoreCase("hola mundo")){
            System.out.println("Son iguales sin importar Mayusculas");
        }
        
        //charAt --> Permite obtener un caracter en una posicion especifica
        System.out.print("Me debe devolver la posicion 5 que es --> ");
        System.out.println(str.charAt(5));
        for (int i = 0; i < str.length(); i++) {
            System.out.print(str.charAt(i) + " ");
        }
        
        //length --> Permitir obtener el largo de un String
        System.out.println("\nEl largo del String es: " + str.length());
        
        //toCharArray --> Permite convertir de un String a un arreglo de char
        char[] letras = str.toCharArray();
        letras[5] = 'N';
//        String x = "";
//        for (int i = 0; i < letras.length; i++) {
//            x += letras[i];
//            System.out.println(letras[i]);
        //new String(char[]) --> Permite crear un String a partir de un arreglo
        //de char
        String x = new String(letras);
        System.out.println("x --> " + x);
        
        //indexOf --> Permite buscar una letra o frase
        System.out.println("Devuelve la posicion donde comienza la frase (Mundo)"
                + " osea la posicion: " + str.indexOf("Mundo"));
        
        //indexOf (String, num) Permite buscar las letras indicadas a partir del
        //numero que se le indico despues de la ,(coma) y si no lo encuentra
        //retorna un -1
        System.out.println("La letra (o) se encuentra tambien en la posicion "
                + "" +str.indexOf("o",2) + " a partir de la posicion 2");
        
        //contains --> Permite conocer si el String contiene o existe la frase
        //indicada, en caso de poseerla retorna un true, de lo contrario un false
        System.out.println(str.contains("Mundo"));
        System.out.println(str.contains("Hola"));
        System.out.println("Como contiene un espacio al inicio retorna"
                + " un " + str.contains(" Hola"));
        
        //replace --> Reemplaza un textto o char en una frase por la letra o
        //texto indicado en la segunda ,(coma)
        System.out.println("Se reemplaza la o minus por una O Mayus "
                + "--> " + str.replace("o", "O"));
        
        //replaceAll --> Reemplaza una expresion regular, por un texto o
        //caracter definido 
        System.out.println("Sin negacion " + str.replaceAll("[A-Z]", "X"));
        System.out.println("Con negacion " + str.replaceAll("[^A-Z]", "X") + ""
                + " Se debe de poner total atencion que tambien reemplaza los"
                + " espacios");
        
        //matches --> Permite saber si un String cumple con una expresion
        //regular. Retorna un true o false
        
        System.out.println("La funcion.matches = " + str.matches(str));
        
        //split --> Me permite generar un arreglo de String  a partir de un String 
        //y un elemento 
        str = "Azul,Blanco,Rojo,Amarillo,Verde";
        String[]colores = str.split(",");
        for (int i = 0; i < colores.length; i++) {
            System.out.println("Se imprime la lista de colores " + colores[i]);
        }
        
        //substring --> [1,6[ -->
        str = "Hola Mundo";
        //Imprime desde la posicion X(en este caso 2) hasta el final
        System.out.println("Imprime desde la posicion 2: " + str.substring(2));
        //Imprime los ultimos 2 carateres por Indexacion negativa
        System.out.println("Imprime los ultimos dos"
                + " caracteres: " + str.substring(str.length()-2));
        //Imprime desde la posicion X hasta la Y(2 y 7)
        System.out.println("Imprime desde la posicion 2 hasta"
                + " la 7: " + str.substring(2, 7));
        
        //concat(+) -- > Concatenar Strings a la antigua
        System.out.println("Funcion.concat implementada: " + str.concat(", Allan"));
        System.out.println("Sin funcion.concat, es la mismo: " + str + ", Allan");
        
        //toLowerCase --> Convierte todo el texto a minuscula
        System.out.println("Texto a minus: " + str.toLowerCase());
        
        //toUpperCase --> Convierte todo el texto a Mayuscula
        System.out.println("Texto a Mayus: " + str.toUpperCase());
        
        //A PARTIR DE AQUI LA PRACTICA
        System.out.println("\nA partir de aqui comienza la Practica\n");
        PractManipulacionChar o = new PractManipulacionChar();
        o.setFrase("Hola World Again! 05");
        System.out.println(o.getFrase());
        //Cantidad de vocales
        System.out.println("La cantidad de vocales es: " + o.cantVocales());
        System.out.println("La cantidad de numeros es: " + o.cantNumeros());
        System.out.println("La cantidad de Mayusculas es: " + o.cantMayus());
        System.out.println("La frase invertida es: " + o.invertirFrase());
        System.out.println("Frase invertida por metodo del Profe: " + o.invertir());
        System.out.println("Frase de vocales -2: " + o.vocalesDosLetrasAntes());
        }
        
        
        //SI/NO
        //S/N
        //Hombre/Mujer
        //H/M
    
        //Reemplazar todas las vocales por dos letras antes.
        //Ejemplo: Hola
        //         Hr
        
}
