/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulacioncaracteres;

/**
 *
 * @author Fernando
 */
public class PractManipulacionChar {
    private String frase;

    public String getFrase() {
        return frase;
    }

    public void setFrase(String frase) {
        this.frase = frase;
    }
    
    public int cantVocales(){
        int voc = 0;
        for (int i = 0; i < frase.length(); i++) {
             
            if(frase.substring(i, i+1).matches("[aeiouAEIOU]")){
                voc ++;
            }
        }
        return voc;
    }
    
    public int cantNumeros(){
        int nums = 0;
        for (int i = 0; i < frase.length(); i++) {
            if(frase.substring(i, i+1).matches("[0-9]"))
                nums ++;
        }
        return nums;
    } 
    
    public int cantMayus(){
        int mayus = 0;
        for (int i = 0; i < frase.length(); i++) {
            if(frase.substring(i,i+1).matches("[A-Z]")){
                mayus ++;
            }            
        }
        return mayus;
    }
    
    public String invertirFrase(){
        String newFrase = "";
        for (int i = frase.length()-1; i >= 0; i--) {
            newFrase += "" + frase.charAt(i);
        }
        return newFrase;
    }
    
    /**
     * Con este metodo se logra obtener un String invertido
     * @return nueva
     */
    public String invertir(){
        String nueva = "";
        for (int i = 0; i < frase.length(); i++) {
            nueva = frase.charAt(i) + nueva;
            
        }
        return nueva;
    }
    
    public String vocalesDosLetrasAntes(){
        String newFrase = frase;
        for (int i = 0; i < frase.length(); i++) {
            if(newFrase.toLowerCase().charAt(i) == 'a'){
                newFrase = newFrase.replace('a', 'y');
            }if(frase.toLowerCase().charAt(i) == 'e'){
                newFrase = newFrase.replace('e', 'c');
            }if(frase.toLowerCase().charAt(i) == 'i'){
                newFrase = newFrase.replace('i', 'g');
            }if(frase.toLowerCase().charAt(i) == 'o'){
                newFrase = newFrase.replace('o', 'm');
            }if(frase.toLowerCase().charAt(i) == 'u'){
                newFrase = newFrase.replace('u', 's');
            }
        }
        return newFrase;
    }
    
    
    
    //Invertir frase
}
